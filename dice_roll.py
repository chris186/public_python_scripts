#! /usr/bin/python3

import random 
def dice_roll(dice, sides):
    print("Rolling {} d{}'s:".format(dice, sides))
    for i in range(dice):
        print(random.randint(0, sides))

def rolling():
    dice = int(input("How many dice would you like to roll? "))
    sides = int(input("How many sides? "))
    print("You would like to roll {} d{}'s.".format(dice, sides))
    dice_roll(dice, sides)
rolling()