#! /usr/bin/python3


# This runs after holy_grail_answer. This will input the variable answers INTO the list. 
def holy_grail_quest(answers):
	answers[0] = input("What is your name: ") # Enters information into the answer for name
#	print(answers[0])
	holy_grail_what(answers[0])
	answers[1] = input("What is your quest: ") # Enters information into the answer for quest
#	print(answers[1])
	holy_grail_what(answers[1])
	answers[2] = input("What is the windspeed velocity of a sparrow: ") # Enters the information into the answer for sparrow velocity
#	print(answers[2])
	holy_grail_what(answers[2])


def holy_grail_answer():
	answers = [None]*3 # Creates a mutable list, which allows answers to be input.
	print("Answer me these questions three! Or put to the death you'll be!!!!")
	holy_grail_quest(answers) # This runs to give input to the listed variables.
	#print(answers)
	if answers[0] != "" and answers[1] != "" and answers[2] != "": # logic...
		print("So, your name is {}, your quest is {}, and the velocity of a sparrow is {}.".format(answers[0], answers[1], answers[2])) #Answer me these questions three!

def holy_grail_what(thing): # Called by another function, run secondary to holy_grail_quest
	if thing == "What?" or thing == "": #if I don't put anything or I put What? in the input, you get ded.
		print("AUUUGH!") # You died.
		exit() #Exits the script right away.


holy_grail_answer() # This is what I want to run, ultimately. 
