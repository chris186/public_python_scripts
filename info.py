#! /usr/bin/python3.6

from openpyxl import Workbook
from openpyxl import load_workbook

def information(information_answer):
		information_answer[0] = input("What is your name? ")
		information_answer[1] = input("What is your age? ")
		information_answer[2] = input("What is your height? ")
		information_answer[3] = input("What is your weight? Don't lie now: ")
		information_answer[4] = input("What is your profession? ")
		information_answer[5] = input("Cell number? ")
		information_answer[6] = input("Email? ")

def info_write_excel(ans, path):
		headers = ['Name', 'Age', 'Height', 'Weight', 'Job', 'Cell #', 'Email']
		book = load_workbook(path)
		sheet = book.get_sheet_by_name('Clients')
		print(ans)
		sheet.append(ans)
		book.save(path)


def information_results():
		information_answer = [None]*7 # This sets up the list of answers.
		path = '/home/paden/Documents/personal/info.xlsx' # Path to my document. Where the info is stored.
		print("I'm going to ask you some questions. Answer truthfully, or you'll be stoned. To death!\n") #just a bit of fun
		information(information_answer) # Runs the information function, and supplies the answers to it.
		info_write_excel(information_answer, path) # writes to the excel spreadsheet.

if __name__ == '__main__':
	information_results()

# Things to do: 
# 1: Append to file - Done
# 2: Set certain fields to only accept integers and a certain number of them.
# 3: Trying to spell words wrong. - spell check works. - Done
